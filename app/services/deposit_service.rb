class DepositService

  def initialize(account, amount_to_deposit)
    @account = account
    @amount_to_deposit = amount_to_deposit
  end

  def call
    @account.update_attribute :amount, @account.amount + @amount_to_deposit
    Transaction.create(account_id: @account.id, amount: @amount_to_deposit, currency: @account.currency, transaction_type: 'deposit' )
  end
end
