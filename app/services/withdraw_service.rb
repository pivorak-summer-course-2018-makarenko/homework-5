class WithdrawService

  def initialize(account, amount_to_withdraw)
    @account = account
    @amount_to_withdraw = amount_to_withdraw
  end

  def call
    @account.update_attribute :amount, @account.amount - @amount_to_withdraw
    Transaction.create(account_id: @account.id, amount: @amount_to_withdraw, currency: @account.currency, transaction_type: 'withdraw' )
  end
end
