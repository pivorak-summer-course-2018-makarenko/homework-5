Rails.application.routes.draw do
  resources :transactions
  devise_for :users
  root to: 'profile#dashboard'
  resources :accounts
  resources :users

  get '/profile/info' => 'profile#info'
  get '/profile/:account_id/deposit' => 'profile#deposit', as: :deposit
  post '/profile/:account_id/do_deposit' => 'profile#do_deposit', as: :do_deposit

  get '/profile/:account_id/withdraw' => 'profile#withdraw', as: :withdraw
  post '/profile/:account_id/do_withdraw' => 'profile#do_withdraw', as: :do_withdraw


end
