require 'rails_helper'

describe "the sign up process", type: :feature do
  it "signs me up" do
    visit "/users/sign_up"
    fill_in 'Email', with: 'user@example.com',visible: true
    fill_in 'Password', with: 'password',visible: true
    fill_in 'Password confirmation', with: 'password',visible: true
    click_button 'Sign up'
    expect(page).to have_content 'Welcome! You have signed up successfully.'
  end

  it "Not signs me up" do
    visit "/users/sign_up"
    fill_in 'Email', with: 'user@example.com',visible: true
    fill_in 'Password', with: '1',visible: true
    fill_in 'Password confirmation', with: 'password',visible: true
    click_button 'Sign up'
    expect(page).to have_content 'Password is too short (minimum is 6 characters)'
  end
end
