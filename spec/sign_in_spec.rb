require 'rails_helper'

describe "the sign up process", type: :feature do
  before :each do
  User.create(email: 'user@example.com', password: 'password')
  end
  it "signs me in" do
    visit "/users/sign_in"
    fill_in 'Email', with: 'user@example.com',visible: true
    fill_in 'Password', with: 'password',visible: true
    click_button 'Log in'
    expect(page).to have_content 'Signed in successfully.'
  end

  it "not signs me in" do
    visit "/users/sign_in"
    fill_in 'Email', with: 'user@example.com',visible: true
    fill_in 'Password', with: '123',visible: true
    click_button 'Log in'
    expect(page).to have_content 'Invalid Email or password.'
  end
end
