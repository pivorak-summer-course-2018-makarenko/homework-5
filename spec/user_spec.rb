require 'rails_helper'

describe User,type: :model do
  describe "validation" do
    it "is invalid without name" do
      expect(FactoryBot.build(:user, name: nil).save).to be_truthy
    end
  end
end
